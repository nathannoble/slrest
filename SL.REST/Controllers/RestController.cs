﻿using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Data.Entity.Infrastructure;
//using System.Linq;
using System.Net;
//using System.Net.Http;
using System.Web.Http;
//using System.Web.Http.ModelBinding;
//using System.Web.Http.OData;
//using System.Web.Http.OData.Routing;
//using ESA.REST.Models;
//using System.Threading.Tasks;

//using System.Data.Entity.Core.EntityClient;
using System.Web.Http.Cors;
//using ESA.REST.Utilities;
//using System.Data.SqlClient;
using SalesforceSharp;
using SalesforceSharp.Security;
//using Newtonsoft.Json.Linq;
//using Newtonsoft.Json;
using System.Web.Script.Serialization;
//using NUnit.Framework;

namespace SL.REST.Controllers
{
    //[EnableCors(origins: "http://myclient.azurewebsites.net", headers: "*", methods: "*")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RestController : ApiController
    {
        //private Entities db = new Entities();

        [Route("rest/CreateSalesforceLead/{firstName}/{lastName}/{company}/{phone}/{email}/{street}/{city}/{state}/{zip}")]
        public string GetCreateSalesforceLead(string firstName, string lastName, string company, string phone, string email, string street, string city, string state, string zip)
        {

            try
            {
                //< add key = "TokenRequestEndpointUrl" value = "https://login.salesforce.com/services/oauth2/token" />
                //< !--add key = "TokenRequestEndpointUrl" value = "https://test.salesforce.com/services/oauth2/token"-- >

                // Info for Salesforce connected app: CollectLeads
                // Test
                //string clientId = "3MVG9ahGHqp.k2_wd_cfwruWJUlhdBzb4_2tAtYWvWbHoPKEO5uNQqJ9zNPkk705006KFiVGGHhHoDgfSuw0Q";
                //string clientSecret = "3429637444393026546";
                //string username = "sarah@corymeyer.com.nathan";
                //string password = "Teamcory1" + "SKxlvQULbiazy7Lw8ybvNJxnz";
                //$impleLi$ting1 +  "XqqAj9q08FWB6Lwh1xEHviiTJ"

                // Production
                string clientId = System.Configuration.ConfigurationManager.AppSettings["clientKey"];
                string clientSecret = System.Configuration.ConfigurationManager.AppSettings["consumerSecret"];
                string username = System.Configuration.ConfigurationManager.AppSettings["salesForceUserName"];
                string password = System.Configuration.ConfigurationManager.AppSettings["salesForcePassword"];

                string lead_Source_Detail__c = System.Configuration.ConfigurationManager.AppSettings["lead_Source_Detail__c"];
                string leadSource = System.Configuration.ConfigurationManager.AppSettings["leadSource"];
                string created_via__c = System.Configuration.ConfigurationManager.AppSettings["created_via__c"];

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var flow = new UsernamePasswordAuthenticationFlow(clientId, clientSecret, username, password);

                // Test
                //flow.TokenRequestEndpointUrl = "https://test.salesforce.com/services/oauth2/token";

                // Production
                flow.TokenRequestEndpointUrl = System.Configuration.ConfigurationManager.AppSettings["salesForceLogin"];

                var target = new SalesforceClient();
                target.Authenticate(flow);
                //Assert.IsTrue(target.IsAuthenticated);
                if (target.IsAuthenticated == true)
                {
                    //Simple Listing Lead Details:
                    //FirstName LastName
                    //Phone:(###) ###-####
                    //Email: test @example.com
                    // Street: 123 1st St
                    //City: Redding
                    //State: California
                    //PostalCode: 96001
                    //Page Viewed: www.simplelisting.com / example

                    //var json = new
                    //{
                    //    LeadSource = leadSource,
                    //    Lead_Source_Detail__c = lead_Source_Detail__c,
                    //    Created_via__c = created_via__c,
                    //    FirstName = firstName,
                    //    LastName = lastName,
                    //    Company = company,
                    //    Phone = phone,
                    //    Email = email,
                    //    Agency_Type__c = "Seller",
                    //    Street = street,
                    //    City = city,
                    //    PostalCode = zip,
                    //    State = state
                    //};
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //string message__c = serializer.Serialize(json);

                    string message__c = firstName + " " + lastName + Environment.NewLine;
                    message__c += phone + Environment.NewLine;
                    message__c += email + Environment.NewLine;
                    message__c += street + Environment.NewLine;
                    message__c += city + Environment.NewLine;
                    message__c += state + Environment.NewLine;
                    message__c += zip + Environment.NewLine;

                    //"LastName":"Faynan",
                    //"FirstName":"Sarahfay",
                    //"Company":"x",
                    //"Country":"United States",
                    //"StateCode":"CA",
                    //"CountryCode":"US",
                    //"Phone":"(325) 669-1667",
                    //"Description":"The caller, an agent in Abilene,TX; and has been trying to reach Cory for the past 3 days regarding a house in Readington, CA (her uncle's house). If she doesn't hear back by tomorrow, she'll have to go for another company, she states. Please Call her back as soon as possible.",
                    //"LeadSource":"Other Source",
                    //"OwnerId":"00G39000005HqB2",
                    //"Lead_Source_Detail__c":"Call-in",
                    //"Message__c":"Sarahfay\nCall from (325) 695-3730 to (866) 859-4138 \nAnswered by Sam C\n05 Oct, 12:56 PM PDT\nCall Type\nCall from Agent\nPhone\n(325) 669-1667 \nAddress 1\ndeclined\nCity\ndeclined\nState\nZip\/Postal Code\n*\nMessage\nThe caller, an agent in Abilene,TX; and has been trying to reach Cory for the past 3 days regarding a house in Readington, CA (her uncle's house). If she doesn't hear back by tomorrow, she'll have to go for another company, she states. Please Call her back as soon as possible.\nCall for (if any)\nRichard and Kathy Look\nFirst Name\nSarahfay\nLast Name\nFaynan\n01:04 PM PDT, Message Taken \nDid we deliver happiness? \nhttps:\/\/maps.google.com\/maps\/place\/Abilene,%20TX  Abilene, TX  \nCaller Details\nSarahfay \n(325) 

                    var record = new
                    {
                        //Simple_Listing__c = true,
                        LeadSource = leadSource, //this seems to cause a deleted duplicate
                        Lead_Source_Detail__c = lead_Source_Detail__c, //bad value for restricted picklist field: Simple Listing
                        Created_via__c = created_via__c, // addition with no problem
                        FirstName = firstName,
                        LastName = lastName,
                        Company = company,
                        Phone = phone,
                        //Address = address,
                        Email = email,
                        Agency_Type__c = "Seller",
                        Street = street,
                        City = city,
                        PostalCode = zip,
                        State = state,
                        //StateCode = "CA", 
                        //CountryCode = "US",
                        //Country = "United States",
                        Message__c = message__c,
                        OwnerId = "00G39000005HqB2"

                    };

                    var id = target.Create("Lead", record);
                    //Assert.IsFalse(String.IsNullOrWhiteSpace(id));

                    if (String.IsNullOrWhiteSpace(id))
                    {
                        return "false";
                    }
                    else
                    {
                        return "true";
                    }
                }
                else
                {
                    return "Authentication failed";
                }

            }
            catch (SalesforceSharp.SalesforceException se)
            {
                return se.Message.ToString();
                //System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                //throw se;
            }
            catch (Exception e)
            {
                return e.Message.ToString();
                //throw;
                //return false;
            }
        }
    }
}
